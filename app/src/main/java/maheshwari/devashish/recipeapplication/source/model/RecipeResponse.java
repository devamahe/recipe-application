package maheshwari.devashish.recipeapplication.source.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecipeResponse{

	@SerializedName("meals")
    private List<Meal> meals;

    public List<Meal> getMeals() {
		return meals;
	}

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

	@Override
 	public String toString(){
		return 
			"RecipeResponse{" + 
			"meals = '" + meals + '\'' + 
			"}";
		}
}