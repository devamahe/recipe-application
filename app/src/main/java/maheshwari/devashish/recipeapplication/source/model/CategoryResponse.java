package maheshwari.devashish.recipeapplication.source.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryResponse {

	@SerializedName("categories")
	private List<Category> categories;

	public void setCategories(List<Category> categories){
		this.categories = categories;
	}

	public List<Category> getCategories(){

		return categories;
	}

	@Override
 	public String toString(){
		return 
			"CategoryResponse{" +
			"categories = '" + categories + '\'' + 
			"}";
		}
}