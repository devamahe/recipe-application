package maheshwari.devashish.recipeapplication.source.service;

import io.reactivex.Single;
import maheshwari.devashish.recipeapplication.source.model.CategoryResponse;
import maheshwari.devashish.recipeapplication.source.model.MealResponse;
import maheshwari.devashish.recipeapplication.source.model.RecipeResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RecipeService {

    @GET("/api/json/v1/1/categories.php")
    Single<CategoryResponse> getAllPhotos();

    @GET("/api/json/v1/1/filter.php")
    Single<MealResponse> getAllMealPhotos(@Query("c") String param1);

    @GET("https://www.themealdb.com/api/json/v1/1/lookup.php")
    Single<RecipeResponse> getRecipe(@Query("i") String param1);


}
