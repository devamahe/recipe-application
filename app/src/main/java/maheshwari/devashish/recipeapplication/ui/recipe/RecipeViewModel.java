package maheshwari.devashish.recipeapplication.ui.recipe;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import maheshwari.devashish.recipeapplication.source.model.Meal;
import maheshwari.devashish.recipeapplication.source.model.RecipeResponse;
import maheshwari.devashish.recipeapplication.source.repository.RecipeRepository;

public class RecipeViewModel extends ViewModel {
    public MutableLiveData<Meal> recipeLiveData = new MutableLiveData<> ();

    private RecipeRepository recipeRepository;

    @Inject
    public RecipeViewModel(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    public void getRecipeResults(String recipeId) {

        recipeRepository.getRecipeResults ( recipeId )
                .subscribeOn ( Schedulers.io () )
                .observeOn ( AndroidSchedulers.mainThread () )
                .subscribe ( new SingleObserver<RecipeResponse> () {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(RecipeResponse recipeResponse) {
                        if (recipeResponse.getMeals () != null) {
                            recipeLiveData.postValue ( recipeResponse.getMeals ().get ( 0 ) );
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                } );
    }
}

