package maheshwari.devashish.recipeapplication.ui.meal;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import maheshwari.devashish.recipeapplication.source.model.Meal;
import maheshwari.devashish.recipeapplication.source.model.MealResponse;
import maheshwari.devashish.recipeapplication.source.repository.MealRepository;

public class MealViewModel extends ViewModel {
    public MutableLiveData<List<Meal>> mealLiveData = new MutableLiveData<> ();

    private MealRepository mealRepository;

    @Inject
    public MealViewModel(MealRepository mealRepository) {
        this.mealRepository = mealRepository;
    }

    public void getMealResults(String dishes) {

        mealRepository.getMealResults ( dishes )
                .subscribeOn ( Schedulers.io () )
                .observeOn ( AndroidSchedulers.mainThread () )
                .subscribe ( new SingleObserver<MealResponse> () {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(MealResponse mealResponse) {
                        if (mealResponse.getMeals () != null) {
                            mealLiveData.postValue ( mealResponse.getMeals () );
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                } );


    }

}
