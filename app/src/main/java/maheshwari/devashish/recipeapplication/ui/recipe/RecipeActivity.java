package maheshwari.devashish.recipeapplication.ui.recipe;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import maheshwari.devashish.recipeapplication.R;
import maheshwari.devashish.recipeapplication.di.view_model.ViewModelFactory;
import maheshwari.devashish.recipeapplication.source.model.Meal;

public class RecipeActivity extends AppCompatActivity {

    private ImageView mealPoster;
    private TextView mealTitle;
    private TextView mealArea;
    private TextView mealInstruction;
    private final String MEAL = "meal";
    private TextView mealTags;
    private final int ZERO = 0;
    private TextView ingredientOne;
    private TextView ingredientTwo;
    private TextView ingredientThree;
    private TextView ingredientFour;
    private TextView ingredientFive;
    private TextView ingredientSix;
    private TextView ingredientSeven;
    private TextView ingredientEight;
    private TextView ingredientNine;
    private TextView ingredientTen;
    private TextView ingredientEleven;
    private TextView ingredientTwelve;
    private TextView ingredientThirteen;
    private TextView ingredientFourteen;
    private TextView ingredientFifteen;
    private TextView MeasureOne;
    private TextView MeasureTwo;
    private TextView MeasureThree;
    private TextView MeasureFour;
    private TextView MeasureFive;
    private TextView MeasureSix;
    private TextView MeasureSeven;
    private TextView MeasureEight;
    private TextView MeasureNine;
    private TextView MeasureTen;
    private TextView MeasureEleven;
    private TextView MeasureTwelve;
    private TextView MeasureThirteen;
    private TextView MeasureFourteen;
    private NestedScrollView scrollView;
    private TextView MeasureFifteen;
    private final String RECIPES = "Recipe";
    private final String TRY_AGAIN = "Try Again";

    private RecipeViewModel recipeViewModel;
    private String mealId;
    private ProgressBar spinner;
    final String RECIPE = "recipe";
    private final String NO_CONNECTION = "No Internet Connection";
    private final String INSTRUCTION = "Instruction";
    private final String INGREDIENT = "Ingredient";
    private TextView mealsInstruction;
    private TextView ingredient;
    private Button button;
    private TextView textView;


    @Inject
    ViewModelFactory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        AndroidInjection.inject ( this );
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_recipe );

        setSupportActionBar ( findViewById ( R.id.dish_toolbar ) );
        ActionBar actionBar = getSupportActionBar ();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled ( true );
            actionBar.setTitle ( RECIPES );
        }
        spinner = findViewById ( R.id.progressBar3 );

        mealPoster = findViewById ( R.id.poster_movie );
        scrollView = findViewById ( R.id.item_details );
        mealTitle = findViewById ( R.id.title_id );
        mealArea = findViewById ( R.id.area_id );
        mealsInstruction = findViewById ( R.id.instruction_text_id );
        mealInstruction = findViewById ( R.id.instruction_id );
        mealTags = findViewById ( R.id.taste_tag );
        ingredient = findViewById ( R.id.ingredient_id );
        ingredientOne = findViewById ( R.id.ingredient_id1 );
        ingredientTwo = findViewById ( R.id.ingredient_id2 );
        ingredientThree = findViewById ( R.id.ingredient_id3 );
        ingredientFour = findViewById ( R.id.ingredient_id4 );
        ingredientFive = findViewById ( R.id.ingredient_id5 );
        ingredientSix = findViewById ( R.id.ingredient_id6 );
        ingredientSeven = findViewById ( R.id.ingredient_id7 );
        ingredientEight = findViewById ( R.id.ingredient_id8 );
        ingredientNine = findViewById ( R.id.ingredient_id9 );
        ingredientTen = findViewById ( R.id.ingredient_id10 );
        ingredientEleven = findViewById ( R.id.ingredient_id11 );
        ingredientTwelve = findViewById ( R.id.ingredient_id12 );
        ingredientThirteen = findViewById ( R.id.ingredient_id13 );
        ingredientFourteen = findViewById ( R.id.ingredient_id14 );
        ingredientFifteen = findViewById ( R.id.ingredient_id15 );
        MeasureOne = findViewById ( R.id.amount_id1 );
        MeasureTwo = findViewById ( R.id.amount_id2 );
        MeasureThree = findViewById ( R.id.amount_id3 );
        MeasureFour = findViewById ( R.id.amount_id4 );
        MeasureFive = findViewById ( R.id.amount_id5 );
        MeasureSix = findViewById ( R.id.amount_id6 );
        MeasureSeven = findViewById ( R.id.amount_id7 );
        MeasureEight = findViewById ( R.id.amount_id8 );
        MeasureNine = findViewById ( R.id.amount_id9 );
        MeasureTen = findViewById ( R.id.amount_id10 );
        MeasureEleven = findViewById ( R.id.amount_id11 );
        MeasureTwelve = findViewById ( R.id.amount_id12 );
        MeasureThirteen = findViewById ( R.id.amount_id13 );
        MeasureFourteen = findViewById ( R.id.amount_id14 );
        MeasureFifteen = findViewById ( R.id.amount_id15 );
        button = findViewById ( R.id.button_category_id3 );
        textView = findViewById ( R.id.no_internet_id3 );

        initScreen ();
        button.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                spinner.setVisibility ( View.VISIBLE );
                initScreen ();
            }
        } );


    }

    private void initConnection() {
        if (getIntent ().hasExtra ( MEAL )) {
            Bundle bundle = getIntent ().getBundleExtra ( MEAL );
            mealId = bundle.getString ( RECIPE );
        }

        recipeViewModel = ViewModelProviders.of ( this, viewModelFactory ).get ( RecipeViewModel.class );
        recipeViewModel.getRecipeResults ( mealId );
        recipeViewModel.recipeLiveData.observe ( this, new Observer<Meal> () {
            @Override
            public void onChanged(Meal mealsItems) {
                spinner.setVisibility ( View.GONE );
                mealTags.setText ( mealsItems.getStrTags () );
                mealsInstruction.setText ( mealsItems.getStrInstructions () );
                mealArea.setText ( mealsItems.getStrArea () );
                mealTitle.setText ( mealsItems.getStrMeal () );
                Picasso.get ().load ( mealsItems.getStrMealThumb () ).into ( mealPoster );
                ingredientOne.setText ( mealsItems.getStrIngredient1 () );
                ingredientTwo.setText ( mealsItems.getStrIngredient2 () );
                ingredientThree.setText ( mealsItems.getStrIngredient3 () );
                ingredientFour.setText ( mealsItems.getStrIngredient4 () );
                ingredientFive.setText ( mealsItems.getStrIngredient5 () );
                ingredientSix.setText ( mealsItems.getStrIngredient6 () );
                ingredientSeven.setText ( mealsItems.getStrIngredient7 () );
                ingredientEight.setText ( mealsItems.getStrIngredient8 () );
                ingredientNine.setText ( mealsItems.getStrIngredient9 () );
                ingredientTen.setText ( mealsItems.getStrIngredient10 () );
                ingredientEleven.setText ( mealsItems.getStrIngredient11 () );
                ingredientTwelve.setText ( mealsItems.getStrIngredient12 () );
                ingredientThirteen.setText ( mealsItems.getStrIngredient13 () );
                ingredientFourteen.setText ( mealsItems.getStrIngredient14 () );
                ingredientFifteen.setText ( mealsItems.getStrIngredient15 () );
                MeasureOne.setText ( mealsItems.getStrMeasure1 () );
                MeasureTwo.setText ( mealsItems.getStrMeasure2 () );
                MeasureThree.setText ( mealsItems.getStrMeasure3 () );
                MeasureFour.setText ( mealsItems.getStrMeasure4 () );
                MeasureFive.setText ( mealsItems.getStrMeasure5 () );
                MeasureSix.setText ( mealsItems.getStrMeasure6 () );
                MeasureSeven.setText ( mealsItems.getStrMeasure7 () );
                MeasureEight.setText ( mealsItems.getStrMeasure8 () );
                MeasureNine.setText ( mealsItems.getStrMeasure9 () );
                MeasureTen.setText ( mealsItems.getStrMeasure10 () );
                MeasureEleven.setText ( mealsItems.getStrMeasure11 () );
                MeasureTwelve.setText ( mealsItems.getStrMeasure12 () );
                MeasureThirteen.setText ( mealsItems.getStrMeasure13 () );
                MeasureFourteen.setText ( mealsItems.getStrMeasure14 () );
                MeasureFifteen.setText ( mealsItems.getStrMeasure15 () );
                mealInstruction.setText ( INSTRUCTION );
                ingredient.setText ( INGREDIENT );


            }
        } );
    }

    private void initScreen() {
        if (!isNetworkAvailable ()) {
            spinner.setVisibility ( View.GONE );
            button.setText ( TRY_AGAIN );
            textView.setText ( NO_CONNECTION );
            scrollView.setVisibility ( View.INVISIBLE );
            button.setVisibility ( View.VISIBLE );
            textView.setVisibility ( View.VISIBLE );
        } else {
            button.setVisibility ( View.GONE );
            textView.setVisibility ( View.GONE );
            scrollView.setVisibility ( View.VISIBLE );
            initConnection ();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId () == android.R.id.home) {
            finish ();
            return true;
        }
        return super.onOptionsItemSelected ( item );
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService ( Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo ();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected ();
    }
}
