package maheshwari.devashish.recipeapplication.ui.category;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import maheshwari.devashish.recipeapplication.R;
import maheshwari.devashish.recipeapplication.source.model.Category;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private List<Category> categoryList;
    private Context context;
    private CategoryItemClickListener mCategoryItemClickListener;

    public interface CategoryItemClickListener {
        void onClick(View view, Category category);
    }

    public void setOnItemClickListener(CategoryItemClickListener listener) {
        mCategoryItemClickListener = listener;
    }

    public CategoryAdapter(Context context, List<Category> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from ( parent.getContext () );
        View view = layoutInflater.inflate ( R.layout.recyclerview_item, parent, false );
        return new ViewHolder ( view );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemView.setOnClickListener ( view -> {
            mCategoryItemClickListener.onClick ( view, categoryList.get ( position ) );
        } );
        holder.myTextView.setText ( categoryList.get ( position ).getStrCategory () );
        Picasso.get ().load ( categoryList.get ( position ).getStrCategoryThumb () )
                .placeholder ( (R.drawable.ic_launcher_foreground) )
                .error ( R.drawable.ic_launcher_foreground )
                .into ( holder.coverImage );
    }

    @Override
    public int getItemCount() {
        return categoryList == null ? 0 : categoryList.size ();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView myTextView;
        private final View mView;
        private ImageView coverImage;

        ViewHolder(View itemView) {
            super ( itemView );
            mView = itemView;
            myTextView = itemView.findViewById ( R.id.info_text );
            coverImage = mView.findViewById ( R.id.cover_image );
        }
    }

    public void updateDataSet(List<Category> category) {
        if (category != null) {
            categoryList = category;
            notifyDataSetChanged ();
        }
    }
}

