package maheshwari.devashish.recipeapplication.ui.category;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import maheshwari.devashish.recipeapplication.source.model.Category;
import maheshwari.devashish.recipeapplication.source.model.CategoryResponse;
import maheshwari.devashish.recipeapplication.source.repository.CategoryRepository;

public class CategoryViewModel extends ViewModel {

    public MutableLiveData<List<Category>> categoryLiveData = new MutableLiveData<> ();
    private CategoryRepository categoryRepository;

    @Inject
    public CategoryViewModel(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public void getCategoryResults() {

        categoryRepository.getCategoryResults ()
                .subscribeOn ( Schedulers.io () )
                .observeOn ( AndroidSchedulers.mainThread () )
                .subscribe ( new SingleObserver<CategoryResponse> () {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(CategoryResponse categoryResponse) {
                        if (categoryResponse.getCategories () != null) {
                            categoryLiveData.postValue ( categoryResponse.getCategories () );
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace ();
                    }
                } );


    }
}
