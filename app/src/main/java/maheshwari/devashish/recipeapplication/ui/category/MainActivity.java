package maheshwari.devashish.recipeapplication.ui.category;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import maheshwari.devashish.recipeapplication.R;
import maheshwari.devashish.recipeapplication.di.view_model.ViewModelFactory;
import maheshwari.devashish.recipeapplication.source.model.Category;
import maheshwari.devashish.recipeapplication.ui.meal.MealActivity;


public class MainActivity extends AppCompatActivity {
    private FirebaseAnalytics mFirebaseAnalytics;
    @Inject
    ViewModelFactory viewModelFactory;

    private CategoryAdapter adapter;
    private CategoryViewModel categoryViewModel;
    List<Category> photoList;
    final String TRY_AGAIN = "Try Again";
    private ProgressBar spinner;
    final String NO_CONNECTION = "No Internet Connection";
    final String CATEGORY = "Category";
    final String IMAGE_PATH = "imagePath";
    private Button button;
    final String ARGUMENTS = "arguments";
    private TextView textView;
    final int TWO = 2;
    final int TWENTY = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject ( this );
        super.onCreate ( savedInstanceState );

        mFirebaseAnalytics = FirebaseAnalytics.getInstance ( this );
        setContentView ( R.layout.activity_main );


        ActionBar actionBar = getSupportActionBar ();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled ( false );
            actionBar.setTitle ( CATEGORY );
        }
        spinner = findViewById ( R.id.progressBar1 );
        textView = findViewById ( R.id.no_internet_id );
        button = findViewById ( R.id.button_category_id );

        RecyclerView recyclerView = findViewById ( R.id.rvNumbers );
        recyclerView.setItemViewCacheSize ( TWENTY );
        recyclerView.setLayoutManager ( new GridLayoutManager ( this, TWO ) );
        adapter = new CategoryAdapter ( this, photoList );
        recyclerView.setAdapter ( adapter );
        initScreen ( adapter );

        init ( adapter );

        button.setOnClickListener ( view -> {
            spinner.setVisibility ( View.VISIBLE );
            initScreen ( adapter );
        } );

    }

    private void setCategory(List<Category> category) {
        if (category != null) {
            adapter.updateDataSet ( category );
        } else {
            adapter.updateDataSet ( categoryViewModel.categoryLiveData.getValue () );
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService ( Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo ();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected ();
    }

    private void initScreen(CategoryAdapter adapter) {
        if (!isNetworkAvailable ()) {
            spinner.setVisibility ( View.GONE );
            button.setText ( TRY_AGAIN );
            textView.setText ( NO_CONNECTION );
            button.setVisibility ( View.VISIBLE );
            textView.setVisibility ( View.VISIBLE );
        } else {
            button.setVisibility ( View.GONE );
            textView.setVisibility ( View.GONE );
            init ( adapter );
        }
    }

    private void init(CategoryAdapter adapter) {
        adapter.setOnItemClickListener ( (View view, Category category) -> {
            Bundle bundle = new Bundle ();
            bundle.putString ( IMAGE_PATH, category.getStrCategory () );
            Intent intent = new Intent ( this, MealActivity.class );
            intent.putExtra ( ARGUMENTS, bundle );
            mFirebaseAnalytics.logEvent ( FirebaseAnalytics.Event.SELECT_CONTENT, bundle );
            if (isNetworkAvailable ()) {
                this.startActivity ( intent );
            } else {
                this.startActivity ( intent );

            }
        } );
        categoryViewModel = ViewModelProviders.of ( this, viewModelFactory ).get ( CategoryViewModel.class );
        categoryViewModel.getCategoryResults ();
        categoryViewModel.categoryLiveData.observe ( this, new Observer<List<Category>> () {
            @Override
            public void onChanged(List<Category> categories) {
                spinner.setVisibility ( View.GONE );
                MainActivity.this.setCategory ( categories );
            }
        } );
    }


}




