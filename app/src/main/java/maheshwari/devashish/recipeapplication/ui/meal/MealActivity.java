package maheshwari.devashish.recipeapplication.ui.meal;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import maheshwari.devashish.recipeapplication.R;
import maheshwari.devashish.recipeapplication.di.view_model.ViewModelFactory;
import maheshwari.devashish.recipeapplication.source.model.Meal;
import maheshwari.devashish.recipeapplication.ui.recipe.RecipeActivity;

public class MealActivity extends AppCompatActivity {

    final String TRY_AGAIN = "Try Again";
    private MealViewModel mealViewModel;
    List<Meal> photoList;
    private MealAdapter adapter;
    private Button button;
    String name;
    private ProgressBar spinner;
    final String IMAGE_PATH = "imagePath";
    final String ARGUMENTS = "arguments";
    final String RECIPE = "recipe";
    final String MEAL = "meal";
    final String DISHES = "Dishes";
    private TextView textView;
    final String NO_CONNECTION = "No Internet Connection";
    final int TWO = 2;
    final int TWENTY = 20;
    @Inject
    ViewModelFactory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = getSupportActionBar ();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled ( true );
            actionBar.setTitle ( DISHES );
        }

        AndroidInjection.inject ( this );
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_meal );
        spinner = findViewById ( R.id.progressBar2 );
        button = findViewById ( R.id.button_category_id2 );
        textView = findViewById ( R.id.no_internet_id2 );

        RecyclerView recyclerView = findViewById ( R.id.meal_rv );
        recyclerView.setItemViewCacheSize ( TWENTY );
        recyclerView.setLayoutManager ( new GridLayoutManager ( this, TWO ) );
        adapter = new MealAdapter ( this, photoList );
        recyclerView.setAdapter ( adapter );
        initScreen ( adapter );

        button.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                spinner.setVisibility ( View.VISIBLE );
                initScreen ( adapter );
            }
        } );


    }

    private void setMealsItem(List<Meal> meal) {
        if (meal != null) {
            adapter.updateDataSet ( meal );
        } else {
            adapter.updateDataSet ( mealViewModel.mealLiveData.getValue () );
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId () == android.R.id.home) {
            finish ();
            return true;
        }
        return super.onOptionsItemSelected ( item );
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService ( Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo ();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected ();
    }

    private void initScreen(MealAdapter adapter) {
        if (!isNetworkAvailable ()) {
            spinner.setVisibility ( View.GONE );
            button.setText ( TRY_AGAIN );
            textView.setText ( NO_CONNECTION );
            button.setVisibility ( View.VISIBLE );
            textView.setVisibility ( View.VISIBLE );
        } else {
            button.setVisibility ( View.GONE );
            textView.setVisibility ( View.GONE );
            init ( adapter );
        }
    }

    private void init(MealAdapter adapter) {
        adapter.setOnItemClickListener ( (View view, Meal meal) -> {
            Bundle bundle = new Bundle ();
            bundle.putString ( RECIPE, meal.getIdMeal () );
            Intent intent = new Intent ( this, RecipeActivity.class );
            intent.putExtra ( MEAL, bundle );
            if (isNetworkAvailable ()) {
                this.startActivity ( intent );
            } else {
                this.startActivity ( intent );
            }
        } );

        if (getIntent ().hasExtra ( ARGUMENTS )) {
            Bundle bundle = getIntent ().getBundleExtra ( ARGUMENTS );
            name = bundle.getString ( IMAGE_PATH );
        }

        mealViewModel = ViewModelProviders.of ( this, viewModelFactory ).get ( MealViewModel.class );
        mealViewModel.getMealResults ( name );
        mealViewModel.mealLiveData.observe ( this, new Observer<List<Meal>> () {
            @Override
            public void onChanged(List<Meal> meals) {
                spinner.setVisibility ( View.GONE );
                MealActivity.this.setMealsItem ( meals );
            }
        } );
    }


}




