package maheshwari.devashish.recipeapplication.ui.meal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import maheshwari.devashish.recipeapplication.R;
import maheshwari.devashish.recipeapplication.source.model.Meal;

public class MealAdapter extends RecyclerView.Adapter<MealAdapter.MealViewHolder> {

    private List<Meal> mealList;
    private Context context;
    private MealAdapter.MealItemClickListener mMealItemClickListener;

    public MealAdapter(Context context, List<Meal> mealList) {
        this.context = context;
        this.mealList = mealList;
    }

    public void setOnItemClickListener(MealItemClickListener listener) {
        mMealItemClickListener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull MealViewHolder holder, int position) {
        holder.itemView.setOnClickListener ( view -> {
            mMealItemClickListener.onClick ( view, mealList.get ( position ) );
        } );
        holder.myTextView.setText ( mealList.get ( position ).getStrMeal () );
        Picasso.get ().load ( mealList.get ( position ).getStrMealThumb () )
                .placeholder ( (R.drawable.ic_launcher_foreground) )
                .error ( R.drawable.ic_launcher_foreground )
                .into(holder.coverImage);
    }

    @NonNull
    @Override
    public MealViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from ( parent.getContext () );
        View view = layoutInflater.inflate ( R.layout.mealrecyclerview_item, parent, false );
        return new MealAdapter.MealViewHolder ( view );
    }

    @Override
    public int getItemCount() {
        return mealList == null ? 0 : mealList.size ();
    }

    public void updateDataSet(List<Meal> meals) {
        if (meals != null) {
            mealList = meals;
            notifyDataSetChanged ();
        }
    }

    class MealViewHolder extends RecyclerView.ViewHolder {
        TextView myTextView;
        private final View mView;
        private ImageView coverImage;

        MealViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            myTextView = itemView.findViewById(R.id.meal_info_text);
            coverImage = mView.findViewById(R.id.meal_cover_image);
        }
    }

    public interface MealItemClickListener {
        void onClick(View view, Meal meal);
    }





}
