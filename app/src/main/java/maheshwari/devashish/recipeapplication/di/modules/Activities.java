package maheshwari.devashish.recipeapplication.di.modules;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import maheshwari.devashish.recipeapplication.ui.category.MainActivity;
import maheshwari.devashish.recipeapplication.ui.meal.MealActivity;
import maheshwari.devashish.recipeapplication.ui.recipe.RecipeActivity;

@Module
public abstract class Activities {

    @ContributesAndroidInjector()
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector()
    abstract MealActivity contributeMealActivity();

    @ContributesAndroidInjector()
    abstract RecipeActivity contributeRecipeActivity();
}
