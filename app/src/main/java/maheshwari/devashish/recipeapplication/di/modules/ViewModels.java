package maheshwari.devashish.recipeapplication.di.modules;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import maheshwari.devashish.recipeapplication.di.view_model.ViewModelFactory;
import maheshwari.devashish.recipeapplication.di.view_model.ViewModelKey;
import maheshwari.devashish.recipeapplication.ui.category.CategoryViewModel;
import maheshwari.devashish.recipeapplication.ui.meal.MealViewModel;
import maheshwari.devashish.recipeapplication.ui.recipe.RecipeViewModel;

@Module
public abstract class ViewModels {
    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(CategoryViewModel.class)
    protected abstract ViewModel categoryViewModel(CategoryViewModel categoryViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MealViewModel.class)
    protected abstract ViewModel mealViewModel(MealViewModel mealViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RecipeViewModel.class)
    protected abstract ViewModel recipeViewModel(RecipeViewModel recipeViewModel);
}


