package maheshwari.devashish.recipeapplication.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;
import maheshwari.devashish.recipeapplication.RecipeApp;
import maheshwari.devashish.recipeapplication.di.modules.Activities;
import maheshwari.devashish.recipeapplication.di.modules.Networking;
import maheshwari.devashish.recipeapplication.di.modules.Repositories;
import maheshwari.devashish.recipeapplication.di.modules.ViewModels;

@Component(modules = {
        Activities.class,
        ViewModels.class,
        Repositories.class,
        Networking.class,
        AndroidSupportInjectionModule.class})

@Singleton
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        AppComponent build();
    }

    void inject(RecipeApp recipeApp);
}