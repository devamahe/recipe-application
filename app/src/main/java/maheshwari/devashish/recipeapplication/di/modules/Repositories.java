package maheshwari.devashish.recipeapplication.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import maheshwari.devashish.recipeapplication.source.repository.CategoryRepository;
import maheshwari.devashish.recipeapplication.source.repository.MealRepository;
import maheshwari.devashish.recipeapplication.source.repository.RecipeRepository;
import retrofit2.Retrofit;

@Module
public class Repositories {

    @Provides
    @Singleton
    CategoryRepository getCategoryRepository(Retrofit retrofit) {
        return new CategoryRepository ( retrofit );
    }

    @Provides
    @Singleton
    MealRepository getMealRepository(Retrofit retrofit) {
        return new MealRepository ( retrofit );
    }

    @Provides
    @Singleton
    RecipeRepository getRecipeRepository(Retrofit retrofit) {
        return new RecipeRepository ( retrofit );
    }
}
